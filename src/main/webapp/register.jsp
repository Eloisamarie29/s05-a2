<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register Information</title>
</head>
<body>

	<!-- Displaying what is stored in the session -->
	<h1>Registration Confirmation</h1>
	<p>First Name: <%=session.getAttribute("fname") %></p>
	<p>Last Name: <%=session.getAttribute("lname") %></p>
	<p>Phone: <%=session.getAttribute("phone") %></p>
	<p>Email: <%=session.getAttribute("email") %></p>
	<p>App Discovery: <%=session.getAttribute("discovery") %></p>
	<p>Date of Birth: <%=session.getAttribute("birthDate") %></p>
	<p>User Type: <%=session.getAttribute("type") %></p>
	<p>Description: <%=session.getAttribute("description") %></p>
		
	<!-- Submit buttons for Registration -->
	<form action = "login" method ="post">
		<input type="submit"/>
	</form>
	
	<!-- Return to the Index.jsp -->
	<form action = "index.jsp">
		<input type = "submit" value="Back"/>
	</form>
</body>
</html>
