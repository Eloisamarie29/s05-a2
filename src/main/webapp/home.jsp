<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
</head>
<body>

	<!-- Display the Concatenation of the first Name and the Last Name -->
	<h1>Welcome <%=session.getAttribute("fullName") %>! </h1>
		
	<!-- Display the message base on what is the User type -->
	<%
		//fetch what type of user is the user
		String userType = session.getAttribute("type").toString();
			
		//Condition for what message will prompted
		if(userType.equals("Applicant")){
			out.println("Welcome applicant. You may now start looking for your career opportunity.");
		}
		else{
			out.println("Welcome employer. You may now start browsing applicant profiles.");
		}
		%>
		
</body>
</html>
